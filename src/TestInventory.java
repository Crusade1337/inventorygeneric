import javax.sound.midi.SysexMessage;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestInventory {


    public static void main(String[] ARGS) {

        Inventory<PocketItem, BackpackItem, Weapon> inventory = new Inventory<>();


        Weapon w1 = new Weapon(1000, 100);
        Weapon w2 = new Weapon(80, 100);
        Weapon w3 = new Weapon(101, 70);
        Weapon w4 = new Weapon(469, 100);


        // gun method tests

        inventory.addWeapon("Raygun", w1);
        inventory.addWeapon("M4A1", w2);


        List<Weapon> weaponList = new ArrayList<>();


        weaponList.add(w3);
        weaponList.add(w4);

        List<String> nameList = new ArrayList<>();

        nameList.add("Ak47");
        nameList.add("AWP");


        inventory.addWeapons(nameList, weaponList);


        // weapon method tests
        PocketItem pocket1 = new PocketItem("p1");
        PocketItem pocket2 = new PocketItem("p2");
        PocketItem pocket3 = new PocketItem("p3");

        inventory.addPocketItem(pocket1);

        List<PocketItem> pocketList = new ArrayList<>();

        pocketList.add(pocket2);
        pocketList.add(pocket3);

        inventory.addPocketItems(pocketList);

        // BackpackItem Method tests


        BackpackItem b1 = new BackpackItem("B1");

        inventory.addToBackpack(b1);

        List<BackpackItem> backPackList = new ArrayList<>();

        BackpackItem b2 = new BackpackItem("B333");
        BackpackItem b3 = new BackpackItem("B4");
        BackpackItem b4 = new BackpackItem("B44");
        BackpackItem b5 = new BackpackItem("B5");


        backPackList.add(b2);
        backPackList.add(b3);
        backPackList.add(b4);
        backPackList.add(b5);

        inventory.addAllToBackpack(backPackList);


        inventory.sortBackpack((a,b)-> a.getName().compareTo(b.getName()));



        inventory.printInventory();

        System.out.println("Count items in Backpack with name.length >= 3");
        System.out.println(inventory.countItemsInBackpack((a)-> a.getName().length() >= 3));
        System.out.println("Count items in Backpack with name.length < 3");
        System.out.println(inventory.countItemsInBackpack((a)-> a.getName().length() < 3));


        System.out.println("Take out of Backpack");
        System.out.println(inventory.takeOutOfBackpack().getName());

        System.out.println("Print Weapons after filter");



        Predicate<Weapon> predicate = o-> o.getConditon()>0;
        Function<Weapon, String> function1 = o -> Integer.toString(o.getConditon());
        Map<String,List<Weapon>> test1 =  inventory.getWeaponsGrouped(predicate, function1);

        test1.entrySet().forEach(System.out::println);
    }
}
