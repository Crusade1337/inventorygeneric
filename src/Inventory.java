import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Inventory<Pocket,Backpack,Weapon> {

    private HashSet<Pocket> pocketList;
    private Stack<Backpack> backpackStack;
    private HashMap<String,Weapon> weaponMap;

    public Inventory(){
        pocketList = new HashSet<>();
        backpackStack = new Stack<>();
        weaponMap = new HashMap<>();
    }

    public void addWeapon(String name,Weapon w){
        weaponMap.put(name,w);
    }

    public void addWeapons(List<String> names,List<Weapon> weapons){
        for(int i=0; i < names.size() || i < weapons.size() ;i++){
            weaponMap.put(names.get(i),weapons.get(i));
        }
    }

    public void addPocketItem(Pocket pocketItem){
        pocketList.add(pocketItem);
    }

    public void addPocketItems(List<Pocket> pocketItems){
        for(int i = 0; i < pocketItems.size();i++){
            pocketList.add(pocketItems.get(i));
        }
    }

    public void addToBackpack(Backpack backpackItem){
        backpackStack.add(backpackItem);
    }

    public void addAllToBackpack(List<Backpack> backpackItems){
        for(int i = 0; i< backpackItems.size();i++){
            backpackStack.push(backpackItems.get(i));
        }
    }

    public Backpack takeOutOfBackpack(){
        return backpackStack.pop();
    }

    public int countItemsInBackpack(Predicate<Backpack> filtercriteria){
        return backpackStack.stream()
                .filter(filtercriteria)
                .collect(Collectors.toList())
                .size();
    }

    public List<Backpack> sortBackpack(Comparator<Backpack> compare){
      return   backpackStack.stream()
                .sorted(compare)
                .collect(Collectors.toList());
    }

    public List<Weapon> getWeapons(Predicate<Weapon> filtercriteria, Comparator<Weapon> compare) {

        return weaponMap.values()
                .stream()
                .filter(filtercriteria)
                .sorted(compare)
                .collect(Collectors.toList());
    }

    public Map<String,List<Weapon>> getWeaponsGrouped(Predicate<Weapon> filtercriteria, Function<Weapon,String> function){
        return weaponMap.values()
                .stream()
                .filter(filtercriteria)
                .collect(Collectors.groupingBy(function));
    }


    public Iterator<Weapon> getWeaponIterator(){
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return false;
            }
            @Override
            public Weapon next() {
                return null;
            }
        };
    }

    public Weapon getWeapon(String name){
       return weaponMap.get(name);
    }

    public void printInventory(){
        System.out.println("Pocket Inhalte:");
        System.out.println("////////------------------////////");
        pocketList.forEach(System.out::println);
        System.out.println(" ");

        System.out.println("BackpackItem Inhalte:");
        System.out.println("////////------------------////////");
        backpackStack.forEach(System.out::println);
        System.out.println(" ");

        System.out.println("WeaponMap Inhalte:");
        System.out.println("////////------------------////////");
        weaponMap.entrySet().forEach(System.out::println);
        System.out.println(" ");

    }


    public int getBackpackWeight(Predicate<Weapon> filtercriteria, Function<Weapon,Integer> function){

        return weaponMap.values()
                .stream()
                .filter(filtercriteria)
                .map(function)
                .collect(Collectors.summingInt(i->i+i));

    }
}
