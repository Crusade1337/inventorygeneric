public class BackpackItem{

    private String name;

    public BackpackItem(String name){
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }


}
