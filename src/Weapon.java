import java.util.Map;

public class Weapon<W> {

    private int damage;
    private int conditon;


    public Weapon(int damage,int condition){
        setDamage(damage);
        setConditon(condition);

    }



    public int getConditon() {
        return conditon;
    }

    public int getDamage() {
        return damage;
    }

    public void setConditon(int conditon) {
        this.conditon = conditon;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

}

